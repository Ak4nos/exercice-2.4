package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge {

    private DigitalBadgeMetadata metadata;
    private File badge;
    private Date begin;
    private Date end;
    private String serial;

    /**
     * Constructeur
     * @param metadata
     * @param badge
     * @param begin
     * @param end
     * @param serial
     */
    public DigitalBadge(DigitalBadgeMetadata metadata, File badge, Date begin, Date end, String serial) {
        this.metadata = metadata;
        this.badge = badge;
        this.begin = begin;
        this.end = end;
        this.serial = serial;
    }

    public DigitalBadge(String s, Date begin, Date end, DigitalBadgeMetadata digitalBadgeMetadata, Object serial) {
    }

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return metadata.equals(that.metadata) && badge.equals(that.badge) && serial.equals(that.serial) && end.equals(that.end);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge, serial, end);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                "serial=" + serial +
                "end=" + end+
                '}';
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
